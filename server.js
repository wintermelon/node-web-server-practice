const express = require('express');
const hbs = require('hbs');
const fs = require('fs');

const port = process.env.port || 3000;
var app = express();

hbs.registerPartials(__dirname + '/views/partials');
app.set('view engine', 'hbs');
app.use(express.static(__dirname + '/public'));

app.use((req, res, next) => {
    var now = new Date().toString();
    var log = `${now}: ${req.method} ${req.url}`;

    console.log(log);
    fs.appendFile('server.log', log + '\n', (err) => {
        if(err) {
            console.log('Unable to append to server.log.');
        }
    });
    next();
});

hbs.registerHelper('getCurrentYear', () => {
    return new Date().getFullYear();
});

hbs.registerHelper('screamIt', (message) => {
    return message.toUpperCase();
});

app.get('/', (request, response) => {
    //response.send('<h1>Hello Express!</h1>');
    // response.send({
    //     name: 'andrian',
    //     likes: [
    //         'Programming',
    //         'Eating'
    //     ]
    // });
    response.render('home.hbs', {
        pageTitle: 'Home Page',
        welcomeText: 'Welcome to my page'        
    });
});

app.get('/about', (req, res) => {
    //res.send('About Page: ' + req);
    res.render('about.hbs', {
        pageTitle: 'About Page',
    });
});

app.get('/bad', (req, res) => {
    res.send({
        errorMessage: 'an error occured!'
    });
});

app.listen(port, () => {
    console.log(`Server is up on port ${port}`);
});